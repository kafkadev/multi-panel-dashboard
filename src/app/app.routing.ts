import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard, AdmGuard } from './_guards/index';
//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AdministratorLayoutComponent } from './layouts/administrator-layout.component';
import { CustomerLayoutComponent } from './layouts/customer-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: '/adm/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'adm',
    component: AdministratorLayoutComponent,
    canActivate: [AdmGuard],
    data: {
      title: 'Administrator Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './administrator/administrator.module#AdministratorModule'
       // loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
    ]
  },
  {
    path: 'cust',
    component: CustomerLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Customer Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './customer/customer.module#CustomerModule'
      },
    ]
  },
    {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
