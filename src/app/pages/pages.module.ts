import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { p404Component } from './404.component';
import { p500Component } from './500.component';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { HttpModule } from '@angular/http';
import { PagesRoutingModule } from './pages-routing.module';
import { FormsModule }    from '@angular/forms';
@NgModule({
  imports: [ PagesRoutingModule,HttpModule,CommonModule,FormsModule ],
  declarations: [
    p404Component,
    p500Component,
    LoginComponent,
    RegisterComponent
  ]
})
export class PagesModule { }
