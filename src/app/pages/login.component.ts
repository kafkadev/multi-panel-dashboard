import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Login } from './shared/login.model';
//import { LoginService } from './shared/login.service';
import { AuthenticationService } from '../_services/index';
@Component({
	selector: 'login',
	templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
	login: Login[] = [];
	model: any = {};
	loading = false;
	returnUrl: string;
	constructor(
		private loginService: AuthenticationService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	ngOnInit() {}

	submitLogin() {
		this.loading = true;
		this.loginService.loginPost(this.model.username, this.model.password).subscribe((res) => {
			let currentUserJson: any = localStorage.getItem('currentUser');
			var currentUserParse = JSON.parse(currentUserJson);

			if (currentUserParse.data.responseObject.user_role.admin) {
				this.router.navigate(['/adm/dashboard']);
			} else if (currentUserParse.data.responseObject.user_role.user) {
				this.router.navigate(['/cust/dashboard']);
			} else {
				this.router.navigate(['/pages/login']);
			}
			console.log(this.model);
		});
	}

}

/*    
this.authenticationService.login(this.model.username, this.model.password)
.subscribe(
data => {
this.router.navigate([this.returnUrl]);
},
error => {
//this.alertService.error(error);
this.loading = false;
});
*/