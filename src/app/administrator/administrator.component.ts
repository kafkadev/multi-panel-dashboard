import { Component, OnInit } from '@angular/core';
import { AdmService } from '../_services/adm.service';
@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css'],
  providers: [AdmService]
})
export class AdministratorComponent implements OnInit {

  constructor(private adminService : AdmService) { }

  ngOnInit() {
this.adminService.getAllCourts().subscribe(data => {
  console.log(data);
  
//this.router.navigate([this.returnUrl]);

},
error => {
  console.log(error);
  
//this.alertService.error(error);
//this.loading = false;
});
  }

}