import { TestBed, inject } from '@angular/core/testing';

import { AddClubComponent } from './add-club.component';

describe('a add-club component', () => {
	let component: AddClubComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AddClubComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AddClubComponent], (AddClubComponent) => {
		component = AddClubComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});