import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
import { AdministratorComponent } from './administrator.component';
import { AllCourtsComponent } from './all-courts/all-courts.component';
import { AllClubsComponent } from './all-clubs/all-clubs.component';

import { AddClubComponent } from './add-club/add-club.component';
import { AddCourtComponent } from './add-court/add-court.component';
const routes: Routes = [
  {
    path: '',
    component: AdministratorComponent,
    data: {
      title: 'Administrator'
    }
  },
  {
    path: 'courts',
    component: AllCourtsComponent,
    data: {
      title: 'Tüm Kortlar'
    }
  },
  {
    path: 'clubs',
    component: AllClubsComponent,
    data: {
      title: 'Tüm Kulüpler'
    }
  },
  {
    path: 'add-club',
    component: AddClubComponent,
    data: {
      title: 'Kulüp Ekle'
    }
  },
  {
    path: 'add-court',
    component: AddCourtComponent,
    data: {
      title: 'Kort Ekle'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorRoutingModule {}


