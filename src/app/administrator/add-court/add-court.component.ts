import { Component, OnInit } from '@angular/core';
import { staticsData } from '../../shared/statics-data';
import { AdmService } from '../../_services/adm.service';
@Component({
	selector: 'add-court',
	templateUrl: 'add-court.component.html',
	providers: [AdmService]
})

export class AddCourtComponent implements OnInit {
	constructor(private adminService: AdmService) { }
	ngOnInit() { }

	newCourt(formValue: any) {
		console.log(formValue.value);


		this.adminService.createNewCourt(formValue.value).subscribe(data => {
			console.log(data);
		},
			error => {
				console.log(error);
			});
	
	}

	formState(event) {
		console.log(event.target.value);


	}
}