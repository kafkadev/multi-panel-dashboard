import { TestBed, inject } from '@angular/core/testing';

import { AddCourtComponent } from './add-court.component';

describe('a add-court component', () => {
	let component: AddCourtComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AddCourtComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AddCourtComponent], (AddCourtComponent) => {
		component = AddCourtComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});