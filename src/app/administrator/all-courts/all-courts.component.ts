import { Component, OnInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
@Component({
	selector: 'all-courts',
	templateUrl: 'all-courts.component.html'
})

export class AllCourtsComponent implements OnInit {
courts : any = [];
	constructor(private adminService: AdmService) { }

	ngOnInit() {
		this.adminService.getAllCourts().subscribe(resp => {
			this.courts = resp.data.result;
			console.log(this.courts);
			
		},
			error => {
				console.log(error);
			});
	}
}