import { TestBed, inject } from '@angular/core/testing';

import { AllCourtsComponent } from './all-courts.component';

describe('a all-courts component', () => {
	let component: AllCourtsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AllCourtsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AllCourtsComponent], (AllCourtsComponent) => {
		component = AllCourtsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});