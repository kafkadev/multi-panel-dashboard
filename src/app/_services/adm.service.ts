import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { staticsData } from '../shared/statics-data';
import { Admin } from '../_models/index';
var formDemo = "email=suat%40testmail.com&password=123";
@Injectable()
export class AdmService {
    constructor(private http: Http) { }
    getDemo(){
        return this.http.get('//teniscim.herokuapp.com/court/get_courtes', this.jwt()).map((response: Response) => response.json());
        
    }
    createNewCourt(formValue) {
        return this.http.post(staticsData.api_url + '/court/save_court', formValue, this.jwt()).map((response: Response) => response.json());
    }
    createNewClub(formValue) {
        return this.http.post(staticsData.api_url + '/club/save_club', formValue, this.jwt()).map((response: Response) => response.json());
    }
     getAllCourts() {
        return this.http.get(staticsData.api_url + '/court/get_courtes', this.jwt()).map((response: Response) => response.json());
    }
     getAllClubs() {
        return this.http.get(staticsData.api_url + '/club/get_all_clubs', this.jwt()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    create() {
        return this.http.post(staticsData.api_url + '/user/checkRequestBodyWithoutToken', {}, this.jwt()).map((response: Response) => response.json());
    }

    update(user: Admin) {
        return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    private jwt() {
        let currentUserParse = JSON.parse(localStorage.getItem('currentUser'));
         if (currentUserParse) {
            if (currentUserParse.data.responseObject.token) {
            let headers = new Headers({ 
                "Authorization": currentUserParse.data.responseObject.token,
                'Content-Type': 'application/json'
         });
            return new RequestOptions({ headers: headers});
            }
        }
        
    }
}