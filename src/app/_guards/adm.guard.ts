import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AdmGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            let currentUserJson: any = localStorage.getItem('currentUser');
            var currentUserParse = JSON.parse(currentUserJson);
            if (currentUserParse.data.responseObject.user_role.admin) {
                return true;
            }
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/pages/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}